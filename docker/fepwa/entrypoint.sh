#!/bin/bash
set -e 

until ls -d /app/backfepwa
do
    echo "Waiting for django volume..."
done

if [ "$1" != "test" ]; then
    until python3 manage.py shell -c "from django.db import connection;connection.cursor().execute('SELECT')"
    do
        echo "Waiting for postgres to be ready..."
        sleep 2
    done
fi

if [ "$1" == "daphne" ]; then
    shift;
        echo LAUNCHING ASGI server daphne $*
        exec daphne $*
elif [ "$1" == "celery" ]; then
    shift;
        echo LAUNCHING celery $*
        exec celery $*
else
    echo LAUNCHING python3 manage.py $*
    exec python3 manage.py $*
fi
