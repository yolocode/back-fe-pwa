#!/bin/sh

set -e

# create bar trading database
/usr/bin/psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --set "dbname=$DATABASE_NAME" --set "dbuser=$DATABASE_USER" --set "dbpwd='$DATABASE_PASSWORD'" <<-EOSQL
    CREATE USER :dbuser WITH PASSWORD :dbpwd CREATEDB;
    CREATE DATABASE :dbname;
    GRANT ALL PRIVILEGES ON DATABASE :dbname TO :dbuser;
EOSQL
