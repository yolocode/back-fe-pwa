# Welcome to the magnificent FEPWA project

In this project you'll see unbelievable code, amazing artwork and impressive technologies.

Ok, maybe I'm showing off a little bit. Hehe !!

This backend project is a rest API which is used by the frontend. 
Furthermore, this project is the backoffice for the frontend. 

To access the backoffice, go to https://fepwa.bartradingx.com/admin
If you see the react app with a 404 like page, just refresh the page and normally, you'll see a login page.

To connect the account, use these credentials:
- username : admin
- password : admin

The ids are really tough to find. Safety first as we say.