# PYTHON BASE
#############
FROM python:3.9-slim AS python-base
ARG ubuntu_uid=1000
ARG ubuntu_gid=1000
# Install base system dependencies
RUN set -x && apt-get update && apt-get -y install \
    curl \
    locales \
    wget \
    build-essential \
    libpq-dev \
    postgresql-client \
    vim \
    net-tools \
    gfortran \
    iproute2 \
    telnet
RUN groupadd -g $ubuntu_gid ubuntu && \
    useradd -u $ubuntu_uid -g ubuntu -m ubuntu && \
    echo "ubuntu:ubuntu" | chpasswd && \
    adduser ubuntu sudo
RUN sed -i -e 's/# fr_FR.UTF-8 UTF-8/fr_FR.UTF-8 UTF-8/' /etc/locale.gen && \
    locale-gen
ENV LANG fr_FR.UTF-8
ENV LANGUAGE fr_FR:en
ENV LC_ALL fr_FR.UTF-8

# FEPWA PYTHON BASE
########################
FROM python-base AS fepwa-python-base
COPY ./docker/fepwa/entrypoint.sh /entrypoint.sh
RUN set -x && \
    chmod +x /entrypoint.sh && \
    mkdir -p /static && \
    mkdir /app && \
    chown ubuntu:ubuntu /app
WORKDIR /app
COPY ./backfepwa /app
USER ubuntu:ubuntu
ENTRYPOINT ["/entrypoint.sh"]

# PYTHON PROD BUILD
###################
FROM python-base AS python-build-prod
RUN set -x && mkdir /wheels
COPY ./requirements.txt /wheels/requirements/requirements.txt
WORKDIR /wheels
RUN set -x && \
  pip install --upgrade pip && \
  pip wheel -r requirements/requirements.txt

# FEPWA PYTHON PROD
########################
FROM fepwa-python-base AS fepwa-python
USER root:root
RUN set -x && mkdir /wheels
COPY --from=python-build-prod /wheels /wheels
RUN set -x && \
    pip install -r /wheels/requirements/requirements.txt -f /wheels --no-index
WORKDIR /app
USER ubuntu:ubuntu

FROM nginx AS static-proxy
RUN mkdir /static
COPY --from=fepwa-python /app/static/ /static/
COPY docker/nginx/nginx.conf /etc/nginx/conf.d/default.conf

