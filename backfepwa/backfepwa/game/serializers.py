from rest_framework import serializers
from .models import Game, PlayerGame, Character, AfterGame
from ..user.serializers import PlayerSerializer
from webpush import send_user_notification


class CharacterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Character
        fields = [
            'attack_level',
            'defense_level',
            'base_pv',
            'distance_move',
        ]


class PlayerGameSerializer(serializers.ModelSerializer):
    character = CharacterSerializer(read_only=True)
    player = PlayerSerializer(read_only=True)

    class Meta:
        model = PlayerGame
        fields = [
            'id',
            'player',
            'game',
            'x',
            'y',
            'lasting_pv',
            'character',
            'position_in_game'
        ]


class PlayerGameUpdateSerializer(serializers.ModelSerializer):
    character = CharacterSerializer(read_only=True)

    class Meta:
        model = PlayerGame
        fields = [
            'id',
            'player',
            'game',
            'x',
            'y',
            'lasting_pv',
            'character',
            'position_in_game'
        ]


class GameSerializer(serializers.ModelSerializer):
    player_games = serializers.SerializerMethodField(read_only=True)
    player_games_update = PlayerGameSerializer(many=True, write_only=True)
    creator = PlayerSerializer(read_only=True)

    class Meta:
        model = Game
        fields = [
            'id',
            'state',
            'slug',
            'creator',
            'tour_number',
            'player_number',
            'player_games',
            'player_games_update',
        ]

    def update(self, instance, validated_data):
        player_games = validated_data.pop("player_games_update")
        for player_game in player_games:
            # update player_game
            player_game_to_update = PlayerGame.objects.get(game=player_game["game"], player=player_game["player"])
            for (key, value) in player_game.items():
                setattr(player_game_to_update, key, value)
            player_game_to_update.save()

            # check if the player is dead
            if player_game_to_update.lasting_pv <= 0:
                try:
                    AfterGame.objects.get(player=player_game_to_update.player, game=player_game_to_update.game)
                except:
                    game = player_game_to_update.game
                    players_length = len(PlayerGame.objects.filter(game=game))
                    dead_players_length = len(AfterGame.objects.filter(game=game))
                    AfterGame.objects.create(
                        player=player_game_to_update.player,
                        game=game,
                        final_position=players_length - dead_players_length,
                    )

                    payload = {"head": "So sad", "body": "You failed miserably !!!", "slug": instance.slug}
                    send_user_notification(player_game_to_update.player.user, payload=payload, ttl=1000)

                    # Check if game is ended
                    dead_player_games = AfterGame.objects.filter(game=game)
                    dead_players_length = len(dead_player_games)
                    dead_players = [dead_player_games.player for dead_player_games in dead_player_games]

                    if dead_players_length == players_length - 1:
                        player_winner = PlayerGame.objects.filter(game=instance).exclude(player__in=dead_players)[0].player
                        AfterGame.objects.create(
                            player=player_winner,
                            game=game,
                            final_position=1,
                        )
                        instance.state = Game.ENDED

        if instance.state != Game.ENDED:
            payload = {"head": "Come on", "body": "Hey Buddy, It's your turn to play !!", "slug": instance.slug}
            player_to_play = PlayerGame.objects.get(game=instance, position_in_game=validated_data.get('player_number'))
            send_user_notification(player_to_play.player.user, payload=payload, ttl=1000)

        instance = super(GameSerializer, self).update(instance, validated_data)
        return instance

    def get_player_games(self, obj):
        players = PlayerGame.objects.filter(game=obj)
        serializer = PlayerGameSerializer(players, many=True)
        return serializer.data


class GameUpdateSerializer(GameSerializer):
    player_games_update = PlayerGameUpdateSerializer(many=True, write_only=True)


class AfterGameSerializer(serializers.ModelSerializer):
    player = PlayerSerializer(read_only=True)

    class Meta:
        model = AfterGame
        fields = [
            'id',
            'player',
            'game',
            'final_position',
        ]
