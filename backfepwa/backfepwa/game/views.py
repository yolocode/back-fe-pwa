from random import randint

from django.shortcuts import render, get_object_or_404
from rest_framework.decorators import action

from .models import Game, PlayerGame, Character, AfterGame
from .serializers import GameSerializer, PlayerGameSerializer, AfterGameSerializer, GameUpdateSerializer
from ..user.authentication import SpecificAuthentication
from rest_framework.response import Response
from rest_framework import status, viewsets
from .utils import generate_slug

# Create your views here.
from ..user.models import Player


class Position:
    def __init__(self, x, y):
        self.x = x
        self.y = y


positions_not_movable = [
    Position(0, 0),
    Position(1, 0),
    Position(2, 0),
    Position(2, 1),
    Position(1, 1),
    Position(0, 1),
    Position(0, 2),
    Position(1, 2),
    Position(0, 3),
    Position(5, 3),
    Position(5, 4),
    Position(6, 4),
    Position(6, 3),
    Position(6, 5),
    Position(7, 4),
    Position(8, 4),
    Position(7, 3),
    Position(8, 3),
    Position(9, 3),
    Position(9, 2),
    Position(8, 2),
    Position(7, 2),
    Position(6, 2),
    Position(7, 1),
    Position(8, 1),
    Position(9, 1),
    Position(9, 2),
    Position(10, 2),
    Position(10, 1),
    Position(9, 3),
    Position(11, 1),
    Position(10, 0),
    Position(9, 0),
    Position(11, 0),
    Position(12, 1),
    Position(15, 2),
    Position(16, 2),
    Position(14, 1),
    Position(15, 1),
    Position(16, 1),
    Position(17, 1),
    Position(15, 0),
    Position(16, 0),
    Position(17, 0),
    Position(18, 0),
    Position(14, 4),
    Position(15, 4),
    Position(15, 5),
    Position(14, 5),
    Position(16, 5),
    Position(14, 6),
    Position(15, 6),
    Position(15, 7),
    Position(14, 10),
    Position(13, 10),
    Position(12, 11),
    Position(13, 11),
    Position(14, 11),
    Position(14, 12),
    Position(13, 12),
    Position(12, 12),
    Position(12, 13),
    Position(13, 13),
    Position(14, 13),
    Position(10, 12),
    Position(10, 13),
    Position(9, 13),
    Position(8, 13),
    Position(8, 14),
    Position(9, 14),
    Position(9, 15),
    Position(8, 15),
    Position(7, 15),
    Position(4, 9),
    Position(4, 10),
    Position(3, 10),
    Position(2, 10),
    Position(2, 10),
    Position(1, 11),
    Position(2, 11),
    Position(3, 11),
    Position(4, 11),
    Position(5, 12),
    Position(4, 12),
    Position(3, 12),
    Position(2, 12),
    Position(1, 12),
    Position(0, 12),
    Position(0, 13),
    Position(1, 13),
    Position(2, 13),
    Position(3, 13),
    Position(4, 13),
    Position(4, 14),
    Position(3, 14),
    Position(2, 14),
    Position(1, 14),
    Position(0, 14),
    Position(0, 15),
    Position(1, 15),
    Position(2, 15),
    Position(3, 15),
]


class GameViewSet(viewsets.ModelViewSet):
    queryset = Game.objects.all()
    serializer_class = GameSerializer
    # permission_classes = [IsAccountAdminOrReadOnly]
    authentication_classes = [SpecificAuthentication]

    def create(self, request, *args, **kwargs):
        player = request.player
        slug = generate_slug()
        game = Game.objects.create(creator=player, slug=slug)
        serializer = GameSerializer(game)

        PlayerGame.objects.create(player=player, game=game)

        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def get_serializer_class(self):
        if self.action == 'update':
            return GameUpdateSerializer
        return GameSerializer

    @action(detail=False, methods=['post'], url_path=r'join/(?P<slug>\d+)')
    def join_game(self, request, slug=None):
        if slug is None:
            return Response(status=status.HTTP_404_NOT_FOUND)

        game = get_object_or_404(klass=Game, slug=slug)

        player = request.player

        try:
            PlayerGame.objects.get(game=game, player=player)
        except:
            PlayerGame.objects.create(player=player, game=game)
            return Response(data={"joined": True})

        return Response(status=status.HTTP_409_CONFLICT)

    @action(detail=False, methods=['post'], url_path=r'leave/(?P<slug>\d+)')
    def leave_game(self, request, slug=None):
        if slug is None:
            return Response(status=status.HTTP_404_NOT_FOUND)

        game = get_object_or_404(klass=Game, slug=slug)
        player = request.player
        player_game = get_object_or_404(klass=PlayerGame, game=game, player=player)
        player_game.delete()

        return Response(data={"left": True})

    @action(detail=False, methods=['post'], url_path=r'close/(?P<slug>\d+)')
    def close_game(self, request, slug=None):
        if slug is None:
            return Response(status=status.HTTP_404_NOT_FOUND)

        game = get_object_or_404(klass=Game, slug=slug)

        player = request.player

        if game.creator != player:
            return Response(status=status.HTTP_403_FORBIDDEN)

        game.delete()

        return Response(data={"closed": True})

    @action(detail=False, methods=['post'], url_path=r'start/(?P<slug>\d+)')
    def start_game(self, request, slug=None):
        if slug is None:
            return Response(status=status.HTTP_404_NOT_FOUND)

        game = get_object_or_404(klass=Game, slug=slug)
        player = request.player

        if game.creator != player:
            return Response(status=status.HTTP_403_FORBIDDEN)

        player_games = PlayerGame.objects.filter(game=game)
        character = Character.objects.first()
        index = 0
        for player_game in player_games:
            is_not_movable = True
            while is_not_movable:
                player_game.x = randint(0, 24)
                player_game.y = randint(0, 15)
                is_not_movable_pos = next(
                    (pos for pos in positions_not_movable if pos.x == player_game.x and pos.y == player_game.y), None)
                is_not_movable = is_not_movable_pos is not None
            player_game.position_in_game = index
            player_game.character = character
            player_game.lasting_pv = character.base_pv
            player_game.save()
            index += 1

        game.state = Game.STARTED
        game.save()

        player_games = PlayerGame.objects.filter(game=game)
        pg_serializer = PlayerGameSerializer(player_games, many=True)
        g_serializer = GameSerializer(game)

        return Response(data={"player_games": pg_serializer.data, "game": g_serializer.data})

    @action(detail=False, methods=['get'], url_path=r'by-slug/(?P<slug>\d+)')
    def get_game_by_slug(self, request, slug=None):
        if slug is None:
            return Response(status=status.HTTP_404_NOT_FOUND)

        game = get_object_or_404(klass=Game, slug=slug)

        g_serializer = GameSerializer(game)

        return Response(data={"game": g_serializer.data})

    @action(detail=False, methods=['get'], url_path=r'stats/(?P<slug>\d+)')
    def get_game_stats(self, request, slug=None):
        if slug is None:
            return Response(status=status.HTTP_404_NOT_FOUND)

        game = get_object_or_404(klass=Game, slug=slug)

        after_games = AfterGame.objects.filter(game=game)
        ag_serializer = AfterGameSerializer(after_games, many=True)

        return Response(ag_serializer.data)
    
    @action(detail=False, methods=['get'], url_path=r'my-games')
    def get_game_by_user(self, request):

        pg = PlayerGame.objects.filter(player=request.player)
        games = [pg.game for pg in pg]
        serializer = self.get_serializer(games, many=True)
        return Response(serializer.data)
