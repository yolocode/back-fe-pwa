from django.db import models
from backfepwa.user.models import Player 
# Create your models here.

class Character(models.Model):
    attack_level = models.IntegerField()
    defense_level = models.IntegerField()
    base_pv = models.IntegerField()
    distance_move = models.IntegerField()

class Game(models.Model):

    NOT_STARTED = 1
    STARTED = 2
    ENDED = 3
    STATE_CHOICES = (
        (NOT_STARTED, "Non débuté"),
        (STARTED, "En cours"),
        (ENDED, "Fini"),
    )

    state = models.SmallIntegerField(choices=STATE_CHOICES, default=NOT_STARTED)
    slug = models.CharField(max_length=10, null=True, blank=True)
    creator = models.ForeignKey(Player, on_delete=models.SET_NULL, null=True, blank=True)
    tour_number = models.IntegerField(default=1)
    player_number = models.IntegerField(default=0)

class AfterGame(models.Model):
    player = models.ForeignKey(Player, on_delete=models.SET_NULL, null=True, blank=True)
    game = models.ForeignKey(Game, on_delete=models.SET_NULL, null=True, blank=True)
    final_position = models.IntegerField()

class PlayerGame(models.Model):
    player = models.ForeignKey(Player, on_delete=models.SET_NULL, null=True, blank=True)
    game = models.ForeignKey(Game, on_delete=models.CASCADE, null=True, blank=True)
    x = models.IntegerField(null=True, blank=True)
    y = models.IntegerField(null=True, blank=True)
    lasting_pv = models.IntegerField(null=True, blank=True)
    character = models.ForeignKey(Character, on_delete=models.SET_NULL, null=True, blank=True)
    position_in_game = models.IntegerField(null=True, blank=True)

class Item(models.Model):
    bonus_attack = models.IntegerField(null=True, blank=True)
    bonus_defense = models.IntegerField(null=True, blank=True)
    image_url = models.CharField(max_length=1000, null=True, blank=True)

class GameItem(models.Model):
    item = models.ForeignKey(Item, on_delete=models.SET_NULL, null=True, blank=True)
    game = models.ForeignKey(Game, on_delete=models.SET_NULL, null=True, blank=True)
    player_game = models.ForeignKey(PlayerGame, on_delete=models.SET_NULL, null=True, blank=True)
    x = models.IntegerField(null=True, blank=True)
    y = models.IntegerField(null=True, blank=True)
