import random

def generate_slug():
    numeric = '0123456789'
    code_len = 6
    return ''.join(random.choices(numeric, k=code_len))
