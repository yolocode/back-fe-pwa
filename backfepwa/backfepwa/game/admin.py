from django.contrib import admin
from .models import *
# Register your models here.


admin.site.register(Character)
admin.site.register(Game)
admin.site.register(AfterGame)
admin.site.register(PlayerGame)
admin.site.register(Item)
admin.site.register(GameItem)