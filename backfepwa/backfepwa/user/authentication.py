from django.contrib.auth.models import User
from rest_framework import authentication
from rest_framework import exceptions
from .models import Player

class SpecificAuthentication(authentication.BaseAuthentication):
    def authenticate(self, request):
        token = request.headers.get('Authentication-Token')
        if not token:
            return None

        try:
            player = Player.objects.get(authentication_token=token)
            return (player.user, None)
        except User.DoesNotExist:
            raise exceptions.AuthenticationFailed('No such user')

        