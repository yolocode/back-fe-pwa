
from django.utils.deprecation import MiddlewareMixin
from .models import Player


class AddPlayerToRequestMiddleware(MiddlewareMixin):
    def process_request(self, request):
        if request.headers.get('Authentication-Token', None):
            try:
                request.player = Player.objects.get(authentication_token=request.headers['Authentication-Token'])
            except:
                print('The player does not exist')
        return None
