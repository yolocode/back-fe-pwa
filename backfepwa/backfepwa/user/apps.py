from django.apps import AppConfig


class UserConfig(AppConfig):
    name = 'backfepwa.user'

    def ready(self):
        import backfepwa.user.signals
