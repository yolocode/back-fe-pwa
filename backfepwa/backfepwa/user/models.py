from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Player(models.Model):
    pseudo = models.CharField(max_length=100, null=True, blank=True)
    picture_url = models.CharField(max_length=1000, null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    authentication_token = models.CharField(max_length=50, null=True, blank=True)

