from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from .models import Player
import uuid
from django.contrib.auth.models import User

@receiver(post_save, sender=Player)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        # FIXME : Maybe make something more resilient
        user = User.objects.create(username=str(uuid.uuid4()))
        instance.user = user
        instance.authentication_token = str(uuid.uuid4())
        instance.save()