from django.shortcuts import render
from rest_framework import viewsets, status
from .authentication import SpecificAuthentication
# Create your views here.
from rest_framework.response import Response
from rest_framework.decorators import api_view, authentication_classes
from .models import Player
from .serializers import PlayerSerializer
from .authentication import SpecificAuthentication
from rest_framework.decorators import action
from webpush import send_user_notification
from webpush.views import save_info

@api_view(['POST'])
def authenticate(request):
    authenticated = SpecificAuthentication().authenticate(request)
    if authenticated:
        return Response(data={"authenticated": True, 'new_user': False}, status=200)
    else:
        player = Player.objects.create()
        return Response(data={"authenticated": True, 'new_user': True, 'authentication_token': player.authentication_token}, status=200)

@api_view(['POST'])
@authentication_classes([SpecificAuthentication])
def test(request):
    payload = {"head": "Welcome!", "body": "Hello World"}

    send_user_notification(user=request.user, payload=payload, ttl=1000)
    return Response(status=200)


@api_view(['POST'])
@authentication_classes([SpecificAuthentication])
def save_information(request):
    return save_info(request)

class PlayerViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for viewing and editing accounts.
    """
    queryset = Player.objects.all()
    serializer_class = PlayerSerializer
    authentication_classes = [SpecificAuthentication]

    @action(detail=False, methods=['get'], url_path=r'myself')
    def myself(self, request):
        if request.player:
            serializer = self.get_serializer(request.player)
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(status=status.HTTP_404_NOT_FOUND)
